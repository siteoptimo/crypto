<?php
/**
 * Crypto: Crypto.php
 *
 * @author Koen Van den Wijngaert <koen@siteoptimo.com>
 * @copyright Copyright 2017, OptimoGroup BVBA
 */


namespace SiteOptimo\Crypto;


use SiteOptimo\Crypto\Key\KeyPair;
use SiteOptimo\Crypto\Key\PrivateKey;
use SiteOptimo\Crypto\Key\PublicKey;
use SiteOptimo\Crypto\Exception\CryptoException;

class Crypto implements CryptoInterface
{
    const CIPHER_METHOD = 'AES-256-CTR';
    const HMAC_ALGO = 'sha256';
    const PADDING = OPENSSL_PKCS1_OAEP_PADDING;

    /**
     * {@inheritDoc}
     */
    public static function generateKeyPair()
    {
        return KeyPair::generate();
    }

    /**
     * {@inheritDoc}
     */
    public static function encrypt($message = '', PublicKey $publicKey)
    {
        $ephemeralPrivateKey = random_bytes(32);
        $ephemeralPublicKey  = random_bytes(32);
        $nonce               = base64_encode(random_bytes(16));
        $cipherText          = base64_encode(self::symEncrypt($message, $ephemeralPrivateKey));
        $mac                 = hash_hmac(self::HMAC_ALGO, $nonce.self::SEPARATOR.$cipherText, $ephemeralPublicKey);
        $data                = base64_encode($ephemeralPrivateKey).self::SEPARATOR.base64_encode($ephemeralPublicKey);
        openssl_public_encrypt($data, $keys, $publicKey->getKey(), self::PADDING);
        $keys = base64_encode($keys);

        return implode(self::SEPARATOR, [$nonce, $cipherText, $mac, $keys]);
    }

    /**
     * Symmetric encryption using openssl_encrypt.
     *
     * @param $message
     * @param $key
     *
     * @return string
     * @throws \SiteOptimo\Crypto\Exception\CryptoException
     */
    public static function symEncrypt($message, $key)
    {
        if (mb_strlen($key, '8bit') !== 32) {
            throw new CryptoException('Needs a 256-bit key!');
        }
        $IVSize = openssl_cipher_iv_length(self::CIPHER_METHOD);
        $IV     = openssl_random_pseudo_bytes($IVSize);

        $cipherText = openssl_encrypt(
            $message,
            self::CIPHER_METHOD,
            $key,
            OPENSSL_RAW_DATA,
            $IV
        );

        return $IV.$cipherText;
    }

    /**
     * {@inheritDoc}
     */
    public static function decrypt($encryptedMessage = '', PrivateKey $privateKey)
    {
        list($nonce, $cipherText, $mac, $keys) = explode(self::SEPARATOR, $encryptedMessage);
        openssl_private_decrypt(base64_decode($keys), $decryptedKeys, $privateKey->getKey(), self::PADDING);
        list($ephemeralPrivateKey, $ephemeralPublicKey) = explode(self::SEPARATOR, $decryptedKeys);
        $ephemeralPrivateKey = base64_decode($ephemeralPrivateKey);
        $ephemeralPublicKey  = base64_decode($ephemeralPublicKey);
        $macToVerify         = hash_hmac(self::HMAC_ALGO, $nonce.self::SEPARATOR.$cipherText, $ephemeralPublicKey);

        if ( ! hash_equals($macToVerify, $mac)) {
            throw new CryptoException('Failed to verify hash.');
        }

        $message = self::symDecrypt(base64_decode($cipherText), $ephemeralPrivateKey);

        return $message;
    }

    /**
     * Symmetric decryption using openssl_encrypt.
     *
     * @param $message
     * @param $key
     *
     * @return string
     * @throws \SiteOptimo\Crypto\Exception\CryptoException
     */
    public static function symDecrypt($message, $key)
    {
        if (mb_strlen($key, '8bit') !== 32) {
            throw new CryptoException('Needs a 256-bit key!');
        }
        $IVSize     = openssl_cipher_iv_length(self::CIPHER_METHOD);
        $IV         = mb_substr($message, 0, $IVSize, '8bit');
        $cipherText = mb_substr($message, $IVSize, null, '8bit');

        return openssl_decrypt(
            $cipherText,
            self::CIPHER_METHOD,
            $key,
            OPENSSL_RAW_DATA,
            $IV
        );
    }
}