<?php

/**
 * crypto: KeyPair.php
 *
 * @author Koen Van den Wijngaert <koen@siteoptimo.com>
 * @copyright Copyright 2017, OptimoGroup BVBA
 */

namespace SiteOptimo\Crypto\Key;

use SiteOptimo\Crypto\Exception\CryptoException;

class KeyPair
{
    const BITS = 4096;
    const DIGEST_ALG = 'sha512';
    const TYPE = OPENSSL_KEYTYPE_RSA;

    /**
     * @var PublicKey
     */
    protected $publicKey;
    /**
     * @var PrivateKey
     */
    private $privateKey;

    /**
     * KeyPair constructor.
     *
     * @param $privateKey PrivateKey
     * @param $publicKey PublicKey
     */
    public function __construct(PrivateKey $privateKey, PublicKey $publicKey = null)
    {
        $this->privateKey = $privateKey;

        if (is_null($publicKey)) {
            $publicKey = $this->privateKey->getPublicKey();
        }

        $this->publicKey = $publicKey;
    }

    /**
     * Generates a new KeyPair.
     *
     * @param int $size
     * @param int $keyType
     *
     * @return \SiteOptimo\Crypto\Key\KeyPair
     * @throws \SiteOptimo\Crypto\Exception\CryptoException
     */
    public static function generate($size = self::BITS, $keyType = self::TYPE)
    {
        if ($keyType === OPENSSL_KEYTYPE_RSA && $size < self::BITS) {
            throw new CryptoException('Key size for RSA must be at least 4096 bits.');
        }

        $res = openssl_pkey_new([
            'digest_alg'       => self::DIGEST_ALG,
            'private_key_bits' => $size,
            'private_key_type' => $keyType,
        ]);

        openssl_pkey_export($res, $privateKey);

        $privateKey = new PrivateKey($privateKey);

        return new self(
            $privateKey,
            $privateKey->getPublicKey()
        );
    }

    /**
     * Don't leak information.
     *
     * @return array
     */
    public function __debugInfo()
    {
        return [];
    }

    /**
     * @return \SiteOptimo\Crypto\Key\PublicKey
     */
    public function getPublicKey()
    {
        return $this->publicKey;
    }

    /**
     * @return \SiteOptimo\Crypto\Key\PrivateKey
     */
    public function getPrivateKey()
    {
        return $this->privateKey;
    }
}