<?php

/**
 * crypto: PrivateKey.php
 *
 * @author Koen Van den Wijngaert <koen@siteoptimo.com>
 * @copyright Copyright 2017, OptimoGroup BVBA
 */
namespace SiteOptimo\Crypto\Key;

class PrivateKey
{
    private $data = null;

    /**
     * PrivateKey constructor.
     *
     * @param null $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get associated public key.
     *
     * @return PublicKey
     */
    public function getPublicKey()
    {
        $res        = openssl_pkey_get_private($this->data);
        $keyDetails = openssl_pkey_get_details($res);

        $publicKey = rtrim(str_replace("\n", "\r\n", $keyDetails['key']), "\r\n");

        return new PublicKey($publicKey);
    }

    /**
     * Don't leak information.
     *
     * @return array
     */
    public function __debugInfo()
    {
        return [];
    }

    /**
     * Get the Private Key.
     *
     * @return string
     */
    public function getKey()
    {
        return $this->data;
    }
}