<?php

/**
 * crypto: PublicKey.php
 *
 * @author Koen Van den Wijngaert <koen@siteoptimo.com>
 * @copyright Copyright 2017, OptimoGroup BVBA
 */
namespace SiteOptimo\Crypto\Key;

class PublicKey
{
    private $data = null;

    /**
     * PrivateKey constructor.
     *
     * @param null $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Don't leak information.
     *
     * @return array
     */
    public function __debugInfo()
    {
        return [];
    }

    /**
     * Get the Public Key.
     *
     * @return string
     */
    public function getKey() {
        return $this->data;
    }
}