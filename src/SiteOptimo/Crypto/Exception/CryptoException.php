<?php

/**
 * crypto: CryptoException.php
 *
 * @author Koen Van den Wijngaert <koen@siteoptimo.com>
 * @copyright Copyright 2017, OptimoGroup BVBA
 */
namespace SiteOptimo\Crypto\Exception;

class CryptoException extends \Exception
{

}