<?php
/**
 * Crypto: CryptoInterface.php
 *
 * @author Koen Van den Wijngaert <koen@siteoptimo.com>
 * @copyright Copyright 2017, OptimoGroup BVBA
 */


namespace SiteOptimo\Crypto;


use SiteOptimo\Crypto\Key\PublicKey;
use SiteOptimo\Crypto\Key\PrivateKey;

/**
 * An interface for safe asymmetric cryptography.
 *
 * Interface CryptoInterface
 * @package SiteOptimo\Crypto
 */
interface CryptoInterface
{
    const SEPARATOR = '$';

    /**
     * Generate a Key Pair.
     *
     * @return \SiteOptimo\Crypto\Key\KeyPair
     */
    public static function generateKeyPair();

    /**
     * Encrypts a message.
     *
     * @param string $message
     * @param \SiteOptimo\Crypto\Key\PublicKey $publicKey
     *
     * @return string $encryptedMessage
     */
    public static function encrypt($message = '', PublicKey $publicKey);

    /**
     * Decrypts a message.
     *
     * @param string $encryptedMessage
     * @param \SiteOptimo\Crypto\Key\PrivateKey $privateKey
     *
     * @return string $message
     */
    public static function decrypt($encryptedMessage = '', PrivateKey $privateKey);

    /**
     * Symmetric encryption of a message.
     *
     * @param $message
     * @param $key
     *
     * @return string Encrypted message.
     */
    public static function symEncrypt($message, $key);

    /**
     * Symmetric decryption of a message.
     */
    public static function symDecrypt($message, $key);
}