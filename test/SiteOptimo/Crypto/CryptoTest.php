<?php
/**
 * crypto: CryptoTest.php
 *
 * @author Koen Van den Wijngaert <koen@siteoptimo.com>
 * @copyright Copyright 2017, OptimoGroup BVBA
 */


namespace SiteOptimo\Crypto;


use PHPUnit\Framework\TestCase;

class CryptoTest extends TestCase
{
    public function testEncrypt() {
        $keyPair = Crypto::generateKeyPair();

        $toEncrypt = 'This is a test string to be encrypted.';

        $encrypted = Crypto::encrypt($toEncrypt, $keyPair->getPublicKey());
        $decrypted = Crypto::decrypt($encrypted, $keyPair->getPrivateKey());

        $this->assertEquals($toEncrypt, $decrypted, 'Decrypted and original message should be the same.');
    }
}
